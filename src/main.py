#!/usr/bin/python3

def proba_anniversaire(n_promo, n):

    # Définition de la fonction factorielle pour calculer les arrangements plus tard
    def factorielle(a, n):
        res = a
        for i in range(a + 1, n + 1):
            res *= i
        return res

    # Définition de la fonction build_sets qui décompose un nombre n en toutes les partitions possibles
    # Par exemple 4 donnera: {(4), (3,1), (2,2), (2,1,1), (1,1,1,1)}
    def build_sets(n):
        # Utilisation d'un set pour gérer des tuples uniques (la décomposition doit être unique)
        sets = set()
        # Ajout récursif de tous les décompositions possibles triées par valeur à l'intèrieur des tuples
        sets.add((n, ))
        for i in range(1, n):
            for j in build_sets(n - i):
                sets.add(tuple(sorted((i, ) + j)))
        return sets

    # Cette variable contient donc un set qui contient plusieurs tuples qui correspondent aux décompositions
    sets = build_sets(n_promo)

    # Définition des variables qui vont être utilisé pour sommer les nombres de permuations et ainsi trouver
    # la probabilité finale
    somme_n_sets = 0
    somme_totale = 0

    # On itère donc pour chaque tuple dans sets afin de trouver le nombre de permutations possible par
    # décomposition de n_promo
    for single_set in sets:

        # Au début de l'itération les arrangements valent 1 car vu que le set de départ est composé seulement
        # de n_promo fois 1, la multiplication vaut {n_promo | 1} * {n_promo - 1 | 1} * ... {1 | 1} * (1 / n_promo!) = 1
        arrangements = 1

        # La variable quotient divise les arrangements pour que le nombre de permutations ne prennent pas en compte les
        # permutations de mêmes nombres/valeurs: (1,1,1,1) doit voir son nombre d'arrangements divisé par 4! car il y a 4 fois
        # la valeur 1 (on supprime la redondance)
        quotient = 1

        # Copie de la valeur de n_promo pour pouvoir calculer les arrangements à chaque itération en modifiant cette valeur
        n_promo_copy = n_promo

        # Compteur utilisé pour calculer la suite de nombres identiques dans le set dans le but de mettre à jour la variable
        # quotient, voilà pourquoi les valeurs dans les tuples devaient être triées, je peux ainsi itérer avec une boucle facilement
        compteur = 1

        # On a besoin d'itérer maintenant sur les valeurs contenues dans chaque tuple
        for i in range(len(single_set)):

            # Voici la formule pour les arrangement qui ne sont rien de plus que le produit de plusieurs coefficients binomiaux
            arrangements *= factorielle(1, n_promo_copy) / (factorielle(1, n_promo_copy - single_set[i]) * factorielle(1, single_set[i]))

            # On retire ensuite la valeur du tuple courante dans le nombre de combinaisons possibles
            n_promo_copy -= single_set[i]

            # Cette condition permet de compter la longueur de la suite de valeur courante
            if i > 0 and single_set[i] == single_set[i - 1]:
                compteur += 1

            # Cette condition met à jour la valeur de la variable quotient grâce au compteur utilisé
            if i == len(single_set) - 1 or (i > 0 and single_set[i] != single_set[i - 1]):
                quotient *= 1 / factorielle(1, compteur)
                compteur = 1
 
        # Cette variable contient donc le nombre de permutations de chaque tuple à la fin de la boucle, la formule est la suivante:
        # on multiplie les probabilités des arrangements en les divisant par le quotient qui contient le nombre d'occurences
        # multiples de permutations à supprimer et enfin la factorielle donne les nombre de jours possibles pour chaque groupe
        # de personnes ayant leurs anniversaires le même jour (le premier groupe à 365 possibilités, le deuxieme 364, etc...)
        nb_permutations = factorielle(365 - len(single_set) + 1, 365) * arrangements * quotient

        # C'est le moment de vérifier si il y a le nombre exact n de personnes partageant leur anniversaire le même jour dans le
        # tuple pour pouvoir ajouter ou non le nombre de permutation à la somme des n dans les sets
        if single_set.count(n):
            somme_n_sets += nb_permutations

        # Cette somme est tout simplement le nombre totale de permutations possibles
        somme_totale += nb_permutations

    # La division du nombre de permutations possibles pour "exactement n personnes" par le nombre de permutations totale nous
    # permet d'obtenir la probabilité qu'exactement n personnes partagent un anniversaire parmi une promotion de n_promo personnes
    # tout en accordant aucune importance aux anniversaires des n_promo - n personnes restantes
    return somme_n_sets / somme_totale


print(proba_anniversaire(15, 3))

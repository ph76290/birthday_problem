#!/usr/bin/python3

def proba_anniversaire(n_promo, n):
    # Fonction retournant la probabilité que n personnes de SCIA sur une
    # promotion de n_promo élèves partagent la même date d'anniversaire

    # Définition de la fonction auxiliaire factorielle dont j'aurais besoin par la suite

    def factorielle(a, n):
        res = a
        for i in range(a + 1, n + 1):
            res *= i
        return res

 
    # On a besoin du coefficient binomial pour connaître le nombre de sorties n possibles dans l'arbre de décision
    coefficient_binomial = factorielle(1, n_promo) / (factorielle(1, n) * factorielle(1, n_promo - n))

    # On peut également utiliser le triangle de Pascal ci dessous construit étage par étage pour retrouver ce nombre de sorties n
    triangle_pascal = [1]
    for i in range(n_promo - 1):
        new_triangle_pascal = []
        for j in range(len(triangle_pascal)):
            if j != 0:
                new_triangle_pascal.append(triangle_pascal[j - 1] + triangle_pascal[j])
            else:
                new_triangle_pascal.append(1)
        new_triangle_pascal.append(1)
        triangle_pascal = new_triangle_pascal

    # On peut utiliser le schéma de Bernoulli pour résoudre ce problème
    # J'en suis arrivé à la conclusion qu'il y a deux méthodes différentes en fonction du prédicat de départ:

    # 1ère méthode: Si l'on considère que "exactement n personnes partageant leurs anniversaires le même jour" implique 
    # que toutes les autres personnes (n_promo - n) dans l'ensemble sont toutes nées un jour différent alors le numérateur sera:
    numérateur = factorielle(365 - (n_promo - n), 365)
    # On obtient cela car on sait que n personnes dont nés le même jour, on attribue un anniversaire à ces personnes
    # il reste alors 364 jours possibles pour chacunes des personnes restantes
    # On choisi un des 364 jours une des personnes restante, il reste donc 363 possibilités pour les autres et ainsi de suite
    # Voilà comment on a besoin de multiplier les probabilité de 365 jusqu'à ce que tout le monde ait un anniversaire
    # ce qui donne bien: 365! / 365 - (n_promo - n)!

    # Le dénominateur est égal à 365 (car c'est le nombre de jours possibles pour chacun des anniversaires) puissance n_promo
    # car il faut multiplier les probabilités des anniversaires de chacunes des personnes de la promotion SCIA
    dénominateur = pow(365, n_promo)

    # 2ème méthode: Si cette fois ci on considère que "exactement n personnes partageant leurs anniversaires le même jour" implique
    # que toutes les autres personnes (n_promo - n) dans l'ensemble sont nées n'importe quel autre jour de l'année
    # alors le numérateur sera:
    numérateur = pow(364, n_promo - n)
    # 364 (pour exprimer tous les jours de l'année moins celui qui correspond à l'anniversaire des n personnes) puissance n_promo - n
    # (pour le nombre de personnes restantes)

    # Le dénominateur est presque égal au dénominateur de la première méthodeà la seule différence que l'exposant est n_promo - 1
    # cela s'explique car on la probabilité de l'anniversaire pour la première personne choisie est 1 et non pas 1/365
    dénominateur = pow(365, n_promo - 1)

    # On peut alors calculer la probabilité comme étant numérateur sur le dénominateur
    probabilité = numérateur / dénominateur

    print(triangle_pascal)

    # Le résultat est donc la probabilité calculée, multipliée par le nombre de sorties n possibles dans l'arbre de décision
    # c'est à dire le coefficient_binomial (ou encore la valeur correspondante dans le triangle de Pascal)
    return probabilité * triangle_pascal[n - 1] * 3 # ou encore probabilité * triangle_pascal[n - 1]

#summ = 0
#for i in range(1, 10):
 #   print("yolo")
  #  summ += proba_anniversaire(9, i)

#print(summ)
print(proba_anniversaire(5, 2))

#!/usr/bin/python3

import numpy as np
import matplotlib
import sys

def generate_promo(n_promo):
	return np.random.randint(365, size=n_promo)

def histo_promo(array):
	histo = np.zeros(365)
	for k in array:
		histo[k] += 1
	return histo

def verify(histo, n):
	for i in histo:
		if n == i:
			return True
	return False

def main(n_promo, n, k):
	freq = 0
	for i in range(k):
		promo = generate_promo(n_promo)
		histo = histo_promo(promo)
		freq += 1 if verify(histo, n) else 0
	freq /= k
	print(freq)


if __name__ == "__main__":
	argv = sys.argv
	main(int(argv[1]), int(argv[2]), int(argv[3]))

This project is from an exercise I had to complete.

The purpose is to compute the probability that exactly a number n
of people (chose randomly in a sample of m people) are born the same day.

It is basically an application of the famous "Birthday problem" but with one
difference which is the probability of EXACTLY n people sharing the same birthday
and not "at least".

You can use my function proba_anniversaire(m, n) to compute the probability for a given
n and m.
There is two files because I implemented a main function following the first method I thought which was the best one and another one in the main2.py file.

And in order to verify the results, I also implemented an algorithm simulating the
number of people n sharing a same birthday in a population of m people with k simulations
in the file simulate.py. To execute it:

	$ ./simulate.py [M] [N] [K]
